@extends('layouts.app')
@section('content')
	<link rel="stylesheet" href="{{asset('css/flashcard_show_style.css')}}">
	
	<div class="container">
	<a href="/flashcards" class="btn btn-primary"> Go back</a>
	<h1>{{$card->word}}</h1>
	<h3>{{$card->category}}</h3>
	<div id="cardTranslation"> 
		{{$card->translation}}
	</div>
	<small>Written on {{$card->created_at}}</small>
	<br>
	<a href="/flashcards/{{$card->id}}/edit" class="btn btn-primary">Edit</a>
	</div>
@endsection