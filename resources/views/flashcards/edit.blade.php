@extends('layouts.app')
@section('content')
	<div class="container">
	<h1>Edit Flashcard</h1>
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
			Open modal
		  </button>
	<div class="modal" id="myModal">
			<div class="modal-dialog">
			  <div class="modal-content">
		  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title">Create A Category</h4>
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
		  
				<!-- Modal body -->
				<div class="modal-body">
					<input class="form-control"  type="text" placeholder="your category name"> 
				</div>
		  
				<!-- Modal footer -->
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
		  
			  </div>
			</div>
		  </div>
	{!! Form::open(['action' => ['FlashcardsController@update',$card->id],'method'=>'PUT']) !!}
		<div class="form-group">
			{{Form::label('category','Category')}}
			{{-- text(name,value,attributes) --}}
			{{-- {{Form::select('category', array('E'=>'Environment','L' => 'Large', 'S' => 'Small'),['id'=>'CatList','class'=>'form-control'])}}
			 --}}
			<input type="text" id="CatInput">
			<select id="CatList">
				<option>Environment</option>
				<option>Vehicles</option>
				<option id="NewCategory">New category</option>
			</select>
		</div>
		<div class="form-group">
			{{Form::label('word','Card Word')}}
			{{Form::text('word',$card->word,['class'=>'form-control','placeholder'=>'Type word here'])}}
			{{Form::label('translation','Card Translation')}}
			{{Form::text('translation',$card->translation,['class'=>'form-control','placeholder'=>'Type translation here'])}}
			{{Form::label('image','Card Image')}}
			{{Form::text('image',$card->imageURL,['class'=>'form-control','placeholder'=>'Type translation here'])}}
			{{Form::label('audio','Card Audio')}}
			{{Form::text('audio',$card->audioURL,['class'=>'form-control','placeholder'=>'Type audio here'])}}
			{{Form::label('definition','Card definition')}}
			{{Form::text('definition',$card->definition,['class'=>'form-control','placeholder'=>'Type definition here'])}}
		</div>
		{{-- {{Form::hidden('_method','PUT')}} --}}
		{{Form::submit('submit',['class'=>'btn btn-primary '])}}
	{!! Form::close()  !!}
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#CatInput").hide();
			$("#CatList").on('change',function(){
				let val = this.value;
				let newCat = $("#NewCategory").val();
				if(val==newCat)
				{
					$("#myModal").modal('toggle');
				};
			})
		});
	</script>
@endsection