@extends('layouts.admin')
@section('content')
    {{-- <table class="table" style="border-collapse:collapse;""> --}}
        <table class="table table-condensed" style="border-collapse:collapse;">
        
            <thead>
                <tr><th>&nbsp;</th>
                    <th>Cattegory ID</th>
                    <th>Category Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
            <tr data-toggle="collapse" data-target="#flash{{$category->CatID}}" class="accordion-toggle">
                    <td>{{$category->CatID}}</td>
                    <td>{{$category->CatName}}</td>
                    <td><button class="btn btn-primary">see cards</button></td>
                </tr>
                <tr>
                    <td colspan="12" class="hiddenRow">
                    <div class="accordian-body collapse" id="flash{{$category->CatID}}"> 
                      <table class="table table-striped">
                              <thead>
                                <tr>
                                    <th>CardID</th>
                                    <th>Word</th>
                                    <th>Translation</th>
                                </tr>
                              </thead>
                           </table>
                      
                      </div> </td>
                </tr>
                @endforeach
            </tbody>
        </table>
@endsection