<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'Category';
    public function flashcards()
    {
        return $this->hasMany('App\Flashcard');
    }
}
